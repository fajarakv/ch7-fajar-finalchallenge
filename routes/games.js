const router = require("express").Router();
const gamesCont = require("../controllers/gamesController");
const restrict = require("../middleware/passport");

router.post("/multiplayer",restrict, gamesCont.multiplayer);
router.post("/createrooms",restrict, gamesCont.createRoom);
router.get("/rooms", gamesCont.roomsHistory);

module.exports = router