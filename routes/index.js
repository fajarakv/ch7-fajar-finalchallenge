var express = require('express');
var router = express.Router();

const pageRouter = require("./pages");
const authRouter = require("./auth");
const usersRouter = require("./users");
const gamesRouter = require("./games");

router.use("/", pageRouter);
router.use("/auth", authRouter);
router.use("/user", usersRouter);
router.use("/games", gamesRouter);

module.exports = router;
