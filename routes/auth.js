const router = require("express").Router();
const auth = require("../controllers/authController");
const passport = require("../lib/passport");
const restrict = require("../middleware/passport");

router.post("/register", auth.register);
router.post("/login", auth.login);
router.get("/whoami", restrict, auth.whoami);

module.exports = router;