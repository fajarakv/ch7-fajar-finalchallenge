const router = require("express").Router();
const users = require("../controllers/usersController");
const restrict = require("../middleware/passport");

router.get("/allusers", users.users);


module.exports = router;