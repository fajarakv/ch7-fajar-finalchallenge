const router = require("express").Router();
const page = require("../controllers/pageController");
const passport = require("../lib/passport");
const restrict = require("../middleware/passport");

router.get("/login", page.login);
router.get("/signup", page.signup)
router.get("/rooms", page.rooms);

module.exports = router;