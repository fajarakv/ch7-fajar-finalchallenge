const {rooms} = require("../models")

exports.login = (req,res) => {
  const title = "Login First"

  res.render("login", {success: req.flash("success"), failed: req.flash("failed"), title})
};

exports.signup = (req,res) => {
  const title = "Sign Up Here"

  res.render("register", {success: req.flash("success"), failed: req.flash("failed"), title})
};

exports.rooms = (req,res) => {
  const title = "This is Rooms History";

  rooms.findAll({
    include: ["userRoom"],
    order: [["updatedAt", "asc"]]
  })
  .then((rooms) => {
    res.render("rooms", {title, data: rooms, token: req.flash("token"), username: req.flash("username")});
  })
  
};