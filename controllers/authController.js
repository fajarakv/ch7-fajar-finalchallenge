const {user} = require("../models")

const format = (user) => {
  const { id, username } = user;

  return {
    id,
    username,
    token: user.generateToken(),
  };
};

exports.register = async(req,res) => {
  try {
    user.register(req.body)
    .then(data => {
      req.flash("success", "Register Success.");
      res.redirect("/signup");
    })
    .catch((err) => {
      // res.status(409).json({ status: "Register Failed", msg: err });
      req.flash("failed", `Register failed. ${err}`);
      res.redirect("/signup");
    });
  } catch (err) {
    res.status(500).json({status: "Register Failed", msg: err.message});
  };
};

exports.login = async(req,res) => {
  try{
    user.auth(req.body)
    .then(data =>  {
      // res.json({status: "Login Success", data: format(data)});
      req.flash("token", `${format(data).token}`);
      req.flash("username", `${format(data).username}`);
      res.redirect("/rooms");
      
    })
    .catch((err) => {
      // res.status(401).json({status: "Login Failed", msg: err});
      req.flash("failed", `Login failed. ${err}`);
      res.redirect("/login");
    })
  } catch (err) {
    res.status(500).json({status: "Login Failed", msg: err.message});
  };
};

exports.whoami = (req,res) => {
  const user = req.user.username
  res.json({user})
}