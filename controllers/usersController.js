const {user} = require("../models");

exports.users = (req,res) => {
  user.findAll()
  .then((user) => {
    res.json({ status: "Fetch All Data Success", data: user });
  })
  .catch((err) => {
    res.status(500).json({ status: "Internal Server Error", msg: err });
  });
};