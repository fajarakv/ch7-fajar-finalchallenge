const {user,games,rooms} = require("../models");

const gameSystem = (p1, p2) => {
  if (p1 === "paper") {
    if (p2 === "paper") {
      return "draw";
    }
    if (p2 === "rock") {
      return "win";
    }
    if (p2 === "scissors") {
      return "lose";
    }
  }

  if (p1 === "rock") {
    if (p2 === "rock") {
      return "draw";
    }
    if (p2 === "scissors") {
      return "win";
    }
    if (p2 === "paper") {
      return "lose";
    }
  }
  
  if (p1 === "scissors") {
    if (p2 === "scissors") {
      return "draw";
    }
    if (p2 === "paper") {
      return "win";
    }
    if (p2 === "rock") {
      return "lose";
    };
  };
};

exports.createRoom = async(req,res) => {
  const {room_name} = req.body
  const found = await rooms.findOne({where: {name:room_name}})
  
  if (found) {
    res.status(409).json({message: "Room Name Existed"})
  } else {
    rooms.create({name:room_name})
    .then((data) => {
      res.status(201).json({message: "Create Room Success", data})
    })
  }
}

exports.multiplayer = async(req,res) => {
  const {userId,roomId,result} = req.body;
 
  const gameFound = await games.findOne({where: {roomId:roomId,userId:userId}})

  const newGame = await games.findAll({where: {roomId:roomId}});
  
  if(newGame.length === 0) {
    user.found(req.body)
    .then(() => {
      rooms.found(req.body)

      .then(() => {
        games.create(req.body)

        .then((respond) => {
          res.status(201).json({status: "Create Game Success", respond});
        })
        .catch((err) => {
          res.status(400).json({status: "Create Game Failed", message: err});
        });

      })
      .catch((err) => {
      res.status(400).json({status: "Create Game Failed", message: err});
    })
    .catch((err) => {
    res.status(400).json({status: "Create Game Failed", message: err});
    }) 
  }) 
  .catch((err) => {
  res.status(400).json({status: "Create Game Failed", message: err});
  }) 
   
  } else if (newGame.length === 1) {
    if (newGame.length === 1) {
      if (!gameFound) {
        const gameResult = gameSystem(newGame[0].result, result);

        games.create(req.body)
        .then(() => {
          rooms.update(
            {result: gameResult, player1Id: newGame[0].userId},
            {where: {id: roomId}}
          )
          .then(() => {
            res.json({result: `Player One ${gameResult}`});
          })
          .catch((err) => {
            res.status(400).json({status: "Create Game Failed", message: err});
          });
        }).catch((err) => {
          res.status(400).json({status: "Create Game Failed", message: err});
        });
      } else {
        res.status(400).json({
          status: "Create Game Failed",
          message: "Other Player Turn"
        });
      }
    }
  } else {
    res.status(400).json({
      status: "Create Game Failed",
      message: "Create Another Room"
    });
  }
}

exports.roomsHistory = (req,res) =>{
  rooms.findAll({
    include: [
      "userRoom"
    ]
  })
  .then((rooms) => {
    res.json({ status: "Fetch All Data Success", data: rooms });
  })
  .catch((err) => {
    res.status(500).json({ status: "Internal Server Error", msg: err });
  });
};
