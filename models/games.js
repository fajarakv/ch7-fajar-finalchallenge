'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class games extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.games.belongsTo(models.user, {
        foreignKey: "userId",
        as: "userGames",
      });

      models.games.belongsTo(models.rooms, {
        foreignKey: "roomId",
        as: "roomGames",
      });
    }
  }
  games.init({
    userId: DataTypes.INTEGER,
    roomId: DataTypes.INTEGER,
    result: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'games',
  });
  return games;
};