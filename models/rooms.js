'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class rooms extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.rooms.belongsTo(models.user, {
        foreignKey: "player1Id",
        as: "userRoom",
      });
    }
    
    static found = async({roomId}) => {
      try{    
        const userFound = await rooms.findOne({where: {id:roomId}});
        if(!userFound) return Promise.reject("RoomId not Found");

        return Promise.resolve(userFound)
      } catch (err) {
        return Promise.reject(err);
      }
    }
  }
  rooms.init({
    name: DataTypes.STRING,
    player1Id: DataTypes.INTEGER,
    result: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'rooms',
  });
  return rooms;
};