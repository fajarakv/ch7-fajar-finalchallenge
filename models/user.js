'use strict';
const {Model} = require('sequelize');
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
    static #encrypt = (password) => bcrypt.hashSync(password, 10);

    static register = async({ username, password, name, email, roles }) => {
      try{
        const encryptedPass = this.#encrypt(password);
        
        if(username==="") return Promise.reject("Fill username first");
        if(password==="") return Promise.reject("Fill password first");
        if(email==="") return Promise.reject("Fill email first");
        if(name==="") return Promise.reject("Fill name first");
        
        const unameFound = await user.findOne({where: {username:username}});
        if(unameFound) return Promise.reject("Username registered before");

        const emailFound = await user.findOne({where: {email:email}});
        if(emailFound) return Promise.reject("Email registered before");
        
        return this.create({ username, password: encryptedPass, name, email, roles});
      } catch (err) {
        return Promise.reject(err);
      };
    }

    checkPass = (password) => bcrypt.compareSync(password, this.password);

    generateToken = () => {
      const payload = {
        id: this.id,
        username: this.username
      }

      const secret = "This is Secret";

      const token = jwt.sign(payload,secret)

      return token;
    };

    static auth = async({username,password}) => {
      try{
        const user = await this.findOne({where:{username}})
        if(!user) return Promise.reject("User not found!")

        const isPassValid = user.checkPass(password)

        if(!isPassValid) return Promise.reject("Wrong Password!")

        return Promise.resolve(user)
      } catch (err) {
        return Promise.reject(err);
      };
    };

    static found = async({userId}) => {
      try{    
        const userFound = await user.findOne({where: {id:userId}});
        if(!userFound) return Promise.reject("UserId not Found");

        return Promise.resolve(userFound)
      } catch (err) {
        return Promise.reject(err);
      }
    }
  }
  
  user.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    roles: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user',
  });
  return user;
};